<?php
/*
*
* Template Name: Statična stranica za 'konkursi'
*
* @package WordPress
* @subpackage zzjz
* @since zavod
*
*/

get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="container">

			<!-- KATEGORIJA -->
			<!--
			<div class="post_cat">
			<h4 class="text-uppercase">
			<?php
			$taxonomy = 'category';
			// get the term IDs assigned to post.
			$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
			// separator between links
			$separator = ', ';
			if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {
			$term_ids = implode( ',' , $post_terms );
			$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
			$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
			// display post categories
			echo  $terms;
		}?>
	</h4>
</div>
-->

			<!-- NASLOV -->
			<h3 class="text-center">Otvoren konkurs za poziciju</h3>
			<?php the_title( '<h1 class="entry-title text-uppercase">', '</h1>' ); ?>

			<section role="main">
				<div class="row">
					<!-- SADRŽAJ -->
					<div id="single" class="entry-content col-md-6 col-md-offset-3" role="article">
						<?php the_content(); ?>
					</div><!-- /content column -->
				</div><!-- /ROW -->

			</section><!-- .entry-content -->
		</div><!-- /CONTAINER -->
	</article><!-- /PAGE -->

<?php endwhile; endif; ?>
<?php wp_reset_query(); ?>

<?php get_footer(); ?>
