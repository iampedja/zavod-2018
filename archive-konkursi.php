<?php
/*
*
* The template used for displaying 'konkursi' custom post type
*
* @package WordPress
* @subpackage zzjz_2018
* @since zzjz_2009
*/

get_header(); ?>

<section id="konkursi" class="archive">
  <div class="container">
    <h1 class="text-center">Konkursi</h1>
    <div class="row">
      <?php
      $upitarhive = new WP_Query( array( 'post_type' => 'konkursi' ) );
      $counter = 0;
      if ( $upitarhive->have_posts() ) : while ( $upitarhive->have_posts() ) : $upitarhive->the_post(); ?>

      <div class="col-md-3">
        <article class="thumbnail">
          <!-- DATUM I KATEGORIJA -->
          <div class="entry-meta">
            <p class="pull-left small muted"><time itemprop="datePublished" datetime="<?php echo the_time('Y-m-d'); ?>"><?php the_time('j.n.Y.'); ?></time></p>
            <p class="pull-right small muted"><?php the_category(' '); ?></p>
            <div class="clearfix"></div>
          </div>

          <!-- NASLOV TEKSTA -->
          <header class="entry-header">
            <?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
          </header>
        </article>
      </div>
      <hr class="visible-xs-block" />

      <?php $counter++; if ( $counter % 4 == 0 ) {
        echo '<div class="clearfix visible-md-block visible-lg-block"></div>';
      } ?>
    <?php endwhile; ?>
  <?php endif; ?>
  <?php wp_reset_query(); ?>
</div>
</div>
</section>

<?php get_footer(); ?>
