<?php
/**
* The template used for displaying other news
*
* @package WordPress
* @subpackage zzjz
* @since zzjz
*/
?>

<section id="latestnews">
  <div class="container">
    <h3 class="text-center">Poslednje vesti</h3>
    <div class="row">
      <?php
      $lastnews = new WP_Query('posts_per_page=6');
      $counter = 0;
      if ( $lastnews->have_posts() ) : while ( $lastnews->have_posts() ) : $lastnews->the_post(); ?>

      <div class="col-sm-4">
        <article class="entry">

          <!-- DATUM I KATEGORIJA -->
          <div class="entry-meta">
            <p class="pull-left small muted"><time itemprop="datePublished" datetime="<?php echo the_time('Y-m-d'); ?>"><?php the_time('j.n.Y.'); ?></time></p>
            <p class="small muted">&nbsp;u <?php the_category(' '); ?></p>
            <!-- <div class="clearfix"></div> -->
          </div>

          <!-- NASLOV TEKSTA -->
          <header class="entry-header">
            <?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
          </header>
          
        </article>
      </div>
      <hr class="visible-xs-block" />

      <?php $counter++; if ( $counter % 3 == 0 ) {
        echo '<div class="clearfix visible-md-block visible-lg-block"></div>';
      } ?>
    <?php endwhile; ?>
  <?php endif; ?>
  <?php wp_reset_query(); ?>
</div>

<!-- ARHIVA -->
<div class="row text-center">
  <a href="<?php echo get_page_link(223); ?>" class="btn btn-default btn-lg" role="button">Pogledajte arhivu vesti</a>
</div>
</div>
</section><!-- /#LATESTNEWS -->
