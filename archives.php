<?php
/*
	*
	* Template Name: Arhiva svih tekstova
	*
	* @package WordPress
	* @subpackage zzjz_2018
	* @since zzjz_2009
*/

get_header(); ?>

<article id="archive">
	<div class="container">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="row">

			<section id="month">
				<div class="col-xs-6 col-md-3">
					<h2>Hronološki</h2>
					<ol class="list-unstyled">
						<?php wp_get_archives('show_post_count=1, before='); ?>
					</ol>
				</div>
			</section><!-- /MONTH LIST -->

			<section id="caegories">
				<div class="col-xs-6 col-md-3">
					<h2>Kategorije</h2>
					<ol class="list-unstyled">
						<?php wp_list_categories('title_li='); ?>
					</ol>
				</div>
			</section><!-- /CATEGORY LIST -->
			<div class="clearfix visible-sm visible-xs"></div><hr class="visible-sm visible-xs" />

			<section id="article_list">
				<div class="col-md-6">
					<h2>Tekstovi</h2>
					<ul>
						<?php
						if (function_exists("wp_bs_pagination"))
						{
							wp_bs_pagination();
						}
						?>
						<?php wp_get_archives('type=yearly'); ?>
					</ul>

					<?php
					// clean up after the query and pagination
					wp_reset_postdata();
					?>

				</div>
			</section><!-- /ARTICLE LIST -->

		</div>
	</div>
</article>


<?php get_footer(); ?>
