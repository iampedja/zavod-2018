<?php
/**
 * The template used for displaying content with higher importance and rendered closer to the top of the page
 *
 * @package WordPress
 * @subpackage zzjz
 * @since zzjz
 */
?>

	<section id="important">
		<div class="container">
			<div class="row">
				<!-- JAVNE NABAVKE -->
				<article class="col-md-4">
          <p><i class="fa fa-folder-open fa-3x fa-outline" aria-hidden="true"></i></p>
          <h3>Javne nabavke</h3>
          <p>Pogledajte aktuelne pozive za javne nabavke. Tako&#273;e, možete pogledati i arhivu starih poziva za javne nabavke.</p>
          <a href="<?php echo home_url(); ?>/nabavke/" class="btn btn-default btn-lg" role="button">Pročitajte više</a>
				</article><!-- /JAVNE NABAVKE -->

				<hr class="visible-xs-block" />

				<!-- KONKURSI -->
				<article class="col-md-4">
					<p><i class="fa fa-users fa-3x fa-outline" aria-hidden="true"></i></p>
					<h3>Konkursi</h3>
					<p>Ut sit amet magna orci. Integer sit amet varius ipsum. Ut mattis ullamcorper fermentum. Integer pretium felis sit amet urna volutpat, ac elementum ante interdum.</p>
					<a href="<?php echo home_url(); ?>/konkursi/" class="btn btn-default btn-lg" role="button">Pročitajte više</a>
				</article><!-- /KONKURSI -->

				<hr class="visible-xs-block" />

        <!-- STATISTIKA -->
				<article class="col-md-4">
	        <p><i class="fa fa-file fa-3x fa-outline" aria-hidden="true"></i></p>
	        <h3>Finansijski izveštaji</h3>
	        <p>Fusce ac consectetur enim. Vivamus turpis tellus, malesuada ac erat vitae, pulvinar venenatis turpis. Quisque interdum blandit est id volutpat.</p>
	        <a href="<?php echo get_page_link(283); ?>" class="btn btn-default btn-lg" role="button">Pročitajte više</a>
				</article><!-- /STATISTIKA -->

			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /#important -->
