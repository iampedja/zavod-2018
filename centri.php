
<?php
/*
 * Template Name: Centri / Unutrašnja organizacija
 *
 * @package WordPress
 * @subpackage zzjz
 * @since zavod
 *
 */
get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('hentry'); ?>>
    <div class="container">
        <header>
            <?php the_title( '<h1 class="entry-title text-uppercase">', '</h1>' ); ?>
        </header>
        <p class="lead text-center entry-summary">Organizaciona struktura Zavoda definisana je Pravilnikom o unutrašnjoj organizaciji Zavoda za zaštitu zdravlja Sombor. Ovim dokumentom definisane su organizacione jedinice i rukovođenje u Zavodu.</p>
    </div><!-- /header CONTAINER -->

    <div class="sectors">
        <div class="container entry-content" role="main">
            <section class="row sector higijena">
                <figure class="col-sm-5 col-sm-offset-1 col-xs-3">
                    <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/laboratorija.jpg" alt="Kurac" />
                    <figcaption class="hidden">Bla bla lorem ipsum<br />Credit: <a href="http://pixabay.com/en/microscope-slide-research-close-up-275984/">Pixabay.com</a></figcaption>
                </figure>
                <aside class="col-sm-5 col-xs-9">
                    <h3>Centar za higijenu i humanu ekologiju</h3>
                    <p>Poslovi centra za higijenu i humanu ekologiju, kao oblast delatnosti Zavoda obavljaju se u okviru odseka higijena i zaštita životne sredine, odseka hemijska laboratorija i odseka za <abbr title="dezinfekcija, dezinsekcija i deratizacija">DDD</abbr>.</p>
                    <p><a href="<?php echo get_page_link(368); ?>"></a></p>
                </aside>
                <div class="clearfix"></div>
            </section>
            <section class="row sector kontrola">
               <aside class="col-lg-6">
                    <div>
                        <h3>Centar za kontrolu i prevenciju bolesti </h3>
                    </div>
                </aside>
                <div class="col-lg-6 hidden-xs photo text-hide">
                    <p>slika</p>
                </div>
            </section>
        </div>
    </div>
</article>
    
<?php endwhile; endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>