<?php
/**
* The template for displaying the footer
*
* Contains footer content and the closing of the #main and #page div elements.
*
* @package WordPress
* @subpackage zzjz
* @since zzjz
*/
?>

<footer id="footer" class="hidden-print">
	<div id="mainfooter" class="small">
		<div class="container">
			<div class="row">
				<div id="hcard" class="col-xs-6 col-sm-2 vcard">
					<h5 class="url fn org"><span class="organization-name">Zavod za javno zdravlje</span> <span class="organization-unit">Sombor</span></h5>
					<ul class="list-unstyled">
						<li>
							<ul class="adr">
								<li><span class="street-address">Vojvođanska 47</span></li>
								<li><span class="postal-code">25000</span> <span class="locality">Sombor</span></li>
								<li class="country-name">Srbija</li>
							</ul>
						</li>
						<li class="telephone"><span class="tel fax voice" type="work" href="tel:+00-381-25-412-888">025/412-888</span></li>
						<!-- <li class="telephone"><span class="tel voice" type="work" href="tel:+00-381-25-412-225">025/412-225</span></li> -->
					</ul>
				</div><!-- /HCARD -->
				<!-- RADNO VREME -->
				<div id="work_hours" class="col-xs-6 col-sm-2">
					<h5><span class="organization-name">Radno vreme</span></h5>
					<dl class="dl-horizontal">
						<dt>Pon</dt>
						<dd>07:00 &dash; 14:30</dd>
						<dt>Uto</dt>
						<dd>07:00 &dash; 14:30</dd>
						<dt>Sre</dt>
						<dd>07:00 &dash; 14:30</dd>
						<dt>Čet</dt>
						<dd>07:00 &dash; 14:30</dd>
						<dt>Pet</dt>
						<dd>07:00 &dash; 14:30</dd>
						<dt>Sub</dt>
						<dd>07:00 &dash; 13:00</dd>
						<dt>Ned</dt>
						<dd>Neradan dan</dd>
					</ul>
				</div><!-- /HCARD -->
				<div class="col-xs-6 col-sm-4 col-md-2">
					<h5>O Zavodu</h5>
					<?php
					wp_nav_menu( array(
						'menu'              => 'O Zavodu',
						'theme_location'    => '',
						'depth'             => 1,
						'container'         => '',
						'container_class'   => '',
						'container_id'      => '',
						'menu_class'        => 'list-unstyled',
						'fallback_cb'       => '',
						'walker'            => ''
					) );
					?>
				</div><!-- /O ZAVODU MENU -->
				<div class="col-xs-6 col-sm-4 col-md-2">
					<h5>Informacije</h5>
					<?php
					wp_nav_menu( array(
						'menu'              => 'Informacije',
						'theme_location'    => '',
						'depth'             => 1,
						'container'         => '',
						'container_class'   => '',
						'container_id'      => '',
						'menu_class'        => 'list-unstyled',
						'fallback_cb'       => '',
						'walker'            => ''
					) );
					?>
				</div><!-- /INFO MENU -->
				<div class="visible-xs clearfix"></div>
				<div class="col-sm-4 col-md-4">
					<h5>Ostanite u toku</h5>
					<ul class="list-inline social">
						<li><a class="twitter text-hide" href="https://twitter.com/zzjzsombor" title="Posetite Twitter stranicu Zavoda za javno zdravlje Sombor">Twitter</a></li>
						<li><a class="facebook text-hide" href="https://www.facebook.com/ZavodzajavnozdravljeSombor" title="Postanite prijatelj Zavoda za javno zdravlje Sombor">Facebook</a></li>
					</ul>
				</div>
			</div><!-- /ROW -->
		</div><!-- /FOOTER COLUMNS -->

		<div id="copy" class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-push-6 legal">
					<p>Pročitajte <a href="#">uslove korišćenja</a> i <a href="<?php echo get_page_link(246); ?>">izjavu o privatnosti</a>.</p>
				</div>
				<div class="col-sm-6 col-sm-pull-6">
					<p>&copy;2009&ndash;<?php echo date('Y'); ?> <a href="<?php echo home_url(); ?>" title="Idite na početnu stranicu">Zavod za javno zdravlje Sombor</a>.</p>
				</div>
			</div>

		</div>
	</div>
</footer>

<!-- SCRIPTS: JQUERY, BOOTSTRAP AND THIRD PARTY PLUGINS -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/jquery-3.3.1.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/bootstrap.min.js"></script>
<!-- main.js is for search modal -->
<script src="<?php echo get_stylesheet_directory_uri() ?>/js/main.js"></script>

<!-- INIT -->
<?php if(is_page('kontakt')) : ?>
	<!-- Google API Key (for Kontakt maps) -->
	<script>
	function initMap() {
		var uluru = {lat: 45.779133, lng: 19.110807};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 15,
			center: uluru
		});
		var marker = new google.maps.Marker({
			position: uluru,
			map: map
		});
	}
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQDgZwGco5STZ6DMCvx4GZ5qhwODWDNlw&callback=initMap"
	type="text/javascript"></script>
<?php endif; ?>

<?php wp_footer(); ?>
</body>
</html>
