<?php
/**
* The template used for displaying featured articles and welcome message
*
* @package WordPress
* @subpackage zzjz
* @since zavod
*/
?>

<div class="full-bg vintage">
  <section class="container">
    <div class="row">
      <section class="col-md-6">
        <!-- <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() ?>/img/vintage.jpg" alt="Stara slika Zavoda." /> -->
        <h2 class="small">O Zavodu</h2>
        <p class="lead">Zavod za javno zdravlje je regionalna ustanova koja se bavi preventivnom zdravstvenom delatno&scaron;&cacute;u. Zavod ima jednu mikrobiolo&scaron;ku laboratoriju u Zapadnoba&ccaron;kom okrugu, &ccaron;ije usluge koriste pacijenti sva &ccaron;etri doma zdravlja i Regionalna bolnica u Somboru.</p>
      </section>
      <section class="col-md-3 col-md-offset-2">
        <h3 class="small">Provera nalaza</h3>
        <form>
          <fieldset disabled>
            <div class="form-group">
              <label for="prezime">Prezime</label>
              <input type="text" id="prezime" class="form-control" />
            </div>
            <div class="form-group">
              <label for="brojNaloga">Broj naloga</label>
              <input type="text" id="brojNaloga" class="form-control" />
              <!-- <p class="help-block">Kontrolni broj nalaza možete naći na potvrdi prijema uzorka.</p> -->
            </div>
            <button type="submit" class="btn btn-primary">Proveri</button>
          </form>
        </fieldset>
      </section>
    </div><!-- /ROW -->
  </section><!-- /CONTAINER -->
</div>
