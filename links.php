<?php
/*
 *
 * Stranica za listu linkova
 *
 * @package WordPress
 * @subpackage zzjz
 * @since zavod
 *
 */

get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="container">
			<div id="single" class="col-md-8 col-md-offset-2">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<section class="entry-content" role="main">
					<?php wp_list_bookmarks(); ?>
				</section><!-- .entry-content -->
			</div>
		</div><!-- /CONTAINER -->
	</article><!-- /PAGE -->

	<?php endwhile; endif; ?>
	<?php wp_reset_query(); ?>

<?php get_footer(); ?>
