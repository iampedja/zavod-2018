<?php
/*
 *
 * Template Name: Statična stranica
 *
 * @package WordPress
 * @subpackage zzjz
 * @since zavod
 *
 */

get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="container">

            <header>
                <!-- KATEGORIJA -->
                <div class="post_cat">
                    <h4 class="text-uppercase">
                       <?php
                        $taxonomy = 'category';
                        // get the term IDs assigned to post.
                        $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
                        // separator between links
                        $separator = ', ';
                        if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {
                            $term_ids = implode( ',' , $post_terms );
                            $terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
                            $terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
                            // display post categories
                            echo  $terms;
                        }?>
                    </h4>
                </div>

                <!-- NASLOV -->
                <?php the_title( '<h1 class="entry-title text-uppercase">', '</h1>' ); ?>
			</header>
			
			<section role="main">
				<div class="row">
				    <!-- SADRŽAJ -->
					<div id="single" class="entry-content col-md-6 col-md-offset-1" role="article">
						<?php the_content(); ?>
					</div><!-- /content column -->
					
					<!-- SIDEBAR -->
					<aside class="hidden-print hidden-xs hidden-sm" role="complementary">
						<div id="sidebar" class="col-md-3 entry-meta col-md-offset-1">
							<?php get_template_part( 'content', 'sidebar' ); ?>
						</div><!-- /sidebar column -->
					</aside>
				</div><!-- /ROW -->
				
			</section><!-- .entry-content -->
		</div><!-- /CONTAINER -->
		<section class="adjecent-posts">
            <div class="container">
                <div class="row">
                <?php
                    $next_post = get_next_post();
                    if (!empty( $next_post )): ?>
                    <div class="col-xs-6 next-post text-right">
                        <h4>&larr; Sledeći tekst</h4>
                        <h3><a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo $next_post->post_title; ?></a></h3>
                    </div><!-- /NEXT POST -->
                    <?php endif; ?>
                    <div class="col-xs-6 prev-post <?php if (empty( $next_post )): ?>col-xs-offset-6<?php endif; ?>">
                        <h4>Prethodni tekst &rarr;</h4>
                        <?php
                            $prev_post = get_previous_post();
                            if (!empty( $prev_post )): ?>
                        <h3><a href="<?php echo get_permalink( $prev_post->ID ); ?>"><?php echo $prev_post->post_title; ?></a></h3>
                        <?php endif; ?>
                    </div><!-- /PREV POST -->
                </div><!-- /ADJ-POSTS ROW -->
            </div>
        </section><!-- /SECTION.ADJECENT-POSTS -->
	</article><!-- /PAGE -->

	<?php endwhile; endif; ?>
	<?php wp_reset_query(); ?>

<?php get_footer(); ?>
