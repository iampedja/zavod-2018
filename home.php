<?php
/**
 * The template file for home page
 *
 * @package WordPress
 * @subpackage zzjz
 * @since zavod
 */

get_header(); ?>

<div id="content">

	<!-- <div id="title" class="jumbotron">
		<div class="container">
			<h1 class="text-center"><?php bloginfo('name'); ?></h1>
			<h2><?php bloginfo('description'); ?></h2>
		</div>
	</div> -->

<?php
	// JUMBOTRON
	get_template_part('content','welcome');
?>

<?php
	// Javne nabavke, Finansijski izveštaji, Konkursi
	get_template_part('content','primary');
?>

<?php
    // Statistika, Referentna laboratorija, Članovima srpskog lekarskog društva, Merenje polena i prognoza UV indeksa
    get_template_part('content','secondary');
?>

<?php 
    // Include the latest news template.
    get_template_part( 'content', 'latestnews' );
?>

</div><!-- /CONTENT -->

<?php get_footer(); ?>