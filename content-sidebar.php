<?php
/**
* The template used for displaying sidebar content on single article/post page
*
* @package WordPress
* @subpackage zzjz
* @since zzjz
*/
?>

<div class="entry-meta-header">
	<h2>Informacije o tekstu</h2>
</div>
<!-- Meta box -->
<div class="entry-meta-content">
	<p>Ovaj tekst je objavljen <time class="published" itemprop="datePublished" datetime="<?php echo the_time('Y-m-d'); ?>"><?php the_time('j.n.Y'); ?></time> pod <span class="cat-links"><?php echo get_the_category_list( _x( ' i ','' ,false ) ); ?></span></p>
	<ul class="bookmarks list-inline fa-ul">
		<li class="bookmark"><a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" title="Stalni link ka <?php echo the_title(); ?>"><i class="fa fa-link"></i></a></li>
		<?php if( function_exists( 'wp_print' ) ); ?><li class="print"><?php echo print_link(); ?></li>
	</ul>
</div>
<!-- Lista ostalih tekstova -->
<footer>
	<h3>Ostali tekstovi</h3>
	<ul class="fa-ul list-unstyled small">
		<?php $lastnews = new WP_Query('posts_per_page=5&category_name=vesti'); ?>
		<?php if ( $lastnews->have_posts() ) : while ( $lastnews->have_posts() ) : $lastnews->the_post(); ?>
			<li><a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><i class="fa fa-li fa-file-text"></i><?php the_title(); ?></a></li>
		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>
	</ul>
</footer>
