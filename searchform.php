<!--
<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( '', 'label' ) ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
	</label>
	<input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
</form>
-->

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <label for="search" class="sr-only">
        <span><?php echo _x( '', 'label' ) ?></span>
    </label>
    <input id="search" name="s" type="search" placeholder="<?php echo esc_attr_x( 'Pretraga...', 'placeholder' ) ?>" title="<?php echo esc_attr_x( 'Tražiti:', 'label'); ?>" value="<?php echo get_search_query() ?>" />
    <input type="hidden" id="searchsubmit" /> 
</form>