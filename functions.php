<?php
/*
* Zavod v2: Functions and Definitions
*
* @package WordPress
* @subpackage zzjz
* @since zavod
*/

$my_theme = wp_get_theme( 'zzjz_2018' );
if ( $my_theme->exists() ) :

    // ADDING BOOTSTRAP WP NAV MENU WALKER
    require_once('wp_bootstrap_navwalker.php');

    // ADDING MENU OPTION IN WP ADMIN
    function register_my_menu() {
        register_nav_menu('header-menu',__( 'Header Menu' ));
    }
    add_action( 'init', 'register_my_menu' );
    function register_my_menus() {
        register_nav_menus(
            array(
                'header-menu' => __( 'Header Menu' ),
                'footer-ozavodu-menu' => __( 'Footer O Zavodu Menu' ),
                'footer-info-menu' => __( 'Footer Informacije Menu' )
            )
        );
    }
    add_action( 'init', 'register_my_menus' );



    // REMOVING NASTY MARGIN AT THE TOP CAUSED BY ADMIN BAR
    // add_action('get_header', 'my_filter_head');
    // function my_filter_head() {
    //     remove_action('wp_head', '_admin_bar_bump_cb');
    // }



    // AUTOMATIC ADDING CLASS IMG-RESPONSIVE TO IMAGES
    function add_image_responsive_class($content) {
        global $post;
        $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
        $replacement = '<img$1class="$2 img-responsive"$3>';
        $content = preg_replace($pattern, $replacement, $content);
        return $content;
    }
    add_filter('the_content', 'add_image_responsive_class');



    // ADDING FIGURE AROUND IMG AUTOMATICALLY
    // function filter_ptags_on_images($content){
    //    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
    // }
    // add_filter('the_content', 'filter_ptags_on_images');
    // add_filter( 'image_send_to_editor', 'wp_image_wrap_init', 10, 8 );
    // function wp_image_wrap_init( $html, $id, $caption, $title, $align, $url, $size, $alt ) {
    //     return '<figure id="wp-image-wrap-'. $id .'" class="wp-image-wrap">'. $html .'</div>';
    // }



    // ADDING SEARCH FORM TO NAVIGATION
    /*function menu_search($items){
    $search = '<li class="search">';
    $search .= '<form method="get" id="searchform" action="/">';
    $search .= '<label for="s" class="assistive-text">Search</label>';
    $search .= '<input type="text" class="field" name="s" id="s" placeholder="Search" />';
    $search .= '<input type="submit" class="submit" name="submit" id="searchsubmit" value="Search" />';
    $search .= '</form>';
    $search .= '</li>';

    return $items . $search;
}
add_filter('wp_nav_menu_items','menu_search');*/



// WILL GO TO THE BEGINNING OF THE ARTICLE WHEN CLICKED ON "MORE" LINK, INSTEAD OF SKIPPING PART OF THE ARTICLE ON SINGLE PAGE
//function remove_more_link_scroll( $link ) {
//    $link = preg_replace( '|#more-[0-9]+|', '', $link );
//    return $link;
//}
//add_filter( 'the_content_more_link', 'remove_more_link_scroll' );



// ADDING THUMBNAIL FEATURE
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 480, 300, true ); // default Post Thumbnail dimensions (cropped)
    // additional image sizes
    // delete the next line if you do not need additional image sizes
    // add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
}



// MAXIMUM POST TITLE LENGHT
//function maxWord($title){
//    global $post;
//    $title = $post->post_title;
//    if (str_word_count($title) >= 12 ) //set this to the maximum number of words
//    wp_die( __('Greška: maksimalan broj reči je dvanaest(12)') );
//}
//add_action('publish_post', 'maxWord');


// PAGINATION
// function base_pagination() {
//     global $wp_query;
//     $big = 999999999; // This needs to be an unlikely integer
//     // For more options and info view the docs for paginate_links()
//     // http://codex.wordpress.org/Function_Reference/paginate_links
//     $paginate_links = paginate_links( array(
//         'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
//         'format' => '?paged=%#%',
//         'current' => max( 1, get_query_var('paged') ),
//         'total' => $wp_query->max_num_pages,
//         'prev_next' => false,
//         'type'  => 'array',
//         'prev_next'   => TRUE,
//         'prev_text'    => __('«'),
//         'next_text'    => __('»'),
//     ) );
//     // Display the pagination if more than one page is found
//     if ( $paginate_links ) {
//         echo '<ul class="pagination">';
//         echo $paginate_links;
//         echo '</ul><!--// end .pagination -->';
//     }
// }



/*
* Konkursi u admin meniju
*
* @link http://codex.wordpress.org/Function_Reference/register_post_type
*/

function konkursi() {
    $labels = array(
        'name'               => _x( 'Konkursi', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Konkurs', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Konkursi', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Konkurs', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Dodaj nov', 'book', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Dodaj novi konkurs', 'your-plugin-textdomain' ),
        'new_item'           => __( 'Novi konkurs', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Izmeni konkurs', 'your-plugin-textdomain' ),
        'view_item'          => __( 'Pogledaj konkurse', 'your-plugin-textdomain' ),
        'all_items'          => __( 'Svi konkursi', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Pretraži konkurse', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Matični konkurs:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'Nema pronađenih konkursa.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'Nema pronađenih konkursa u kanti za smeće.', 'your-plugin-textdomain' )
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'konkursi' ),
        'capability_type'    => 'page',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'menu_icon' => 'dashicons-rss',
        'supports'           => array( 'title', 'editor' )
    );
    register_post_type( 'konkursi', $args );
}
add_action( 'init', 'konkursi' );

function my_taxonomies_konkursi() {
    $labels = array(
        'name'              => _x( 'Vrste konkursa', 'taxonomy general name' ),
        'singular_name'     => _x( 'Vrsta konkursa', 'taxonomy singular name' ),
        'search_items'      => __( 'Pretraži vrste konkursa' ),
        'all_items'         => __( 'Sve vrste konkursa' ),
        'parent_item'       => __( 'Parent Product Category' ),
        'parent_item_colon' => __( 'Parent Product Category:' ),
        'edit_item'         => __( 'Izmeni konkurs' ),
        'update_item'       => __( 'Osveži konkurs' ),
        'add_new_item'      => __( 'Dodaj novu vrstu konkursa' ),
        'new_item_name'     => __( 'Nova vrsta konkursa' ),
        'menu_name'         => __( 'Vrste' ),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
    );
    register_taxonomy( 'vrste_konkursa', 'konkursi', $args );
}
add_action( 'init', 'my_taxonomies_konkursi', 0 );



function javne_nabavke() {
    $labels = array(
        'name'               => _x( 'Javne nabavke', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Javna nabavka', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Javne nabavke', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Javna nabavka', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Dodaj novu', 'book', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Dodaj novu javnu nabavku', 'your-plugin-textdomain' ),
        'new_item'           => __( 'Nova javna nabavka', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Izmeni javnu nabavku', 'your-plugin-textdomain' ),
        'view_item'          => __( 'Pogledaj javne nabavke', 'your-plugin-textdomain' ),
        'all_items'          => __( 'Sve javne nabavke', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Pretraži javne nabavke', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Grupacija:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'Nema pronađenih javnih nabavki.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'Nema pronađenih javnih nabavki u kanti za smeće.', 'your-plugin-textdomain' )
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'nabavke' ),
        'capability_type'    => 'page',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'menu_icon' => 'dashicons-cart',
        'supports'           => array( 'title', 'excerpt' )
    );
    register_post_type( 'javne_nabavke', $args );
}
add_action( 'init', 'javne_nabavke' );

function my_taxonomies_nabavke() {
    $labels = array(
        'name'              => _x( 'Vrste javnih nabavki', 'taxonomy general name' ),
        'singular_name'     => _x( 'Vrsta javne nabavke', 'taxonomy singular name' ),
        'search_items'      => __( 'Pretraži vrste javnih nabavki' ),
        'all_items'         => __( 'Sve vrste javnih nabavki' ),
        'parent_item'       => __( 'Parent Product Category' ),
        'parent_item_colon' => __( 'Parent Product Category:' ),
        'edit_item'         => __( 'Izmeni javnu nabavku' ),
        'update_item'       => __( 'Osveži javnu nabavku' ),
        'add_new_item'      => __( 'Dodaj novu vrstu javne nabavke' ),
        'new_item_name'     => __( 'Nova vrsta javne nabavke' ),
        'menu_name'         => __( 'Vrste' ),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
    );
    register_taxonomy( 'vrste_nabavki', 'javne_nabavke', $args );
}
add_action( 'init', 'my_taxonomies_nabavke', 0 );

// ADDING CUSTOM POST TYPE TO JAVNE NABAVKE
function add_nabavke_meta_box() {
    add_meta_box(
        'nabavke_meta_box', // $id
        'Detalji nabavke', // $title
        'show_nabavke_meta_box', // $callback
        'javne_nabavke', // $screen
        'normal', // $context
        'high' // $priority
    );
}
add_action( 'add_meta_boxes', 'add_nabavke_meta_box' );
// STYLING JAVNE NABAVKE META BOX
function show_nabavke_meta_box() {
    global $post;
    $meta = get_post_meta( $post->ID, 'nabavke_fields', true ); ?>
    <input type="hidden" name="nabavke_meta_box_nonce" value="<?php echo wp_create_nonce() ?>"></input>
    <p>
        <label for="nabavke_fields[text]">Input Text</label>
        <input type="text" name="nabavke_fields[text]" id="nabavke_fields[text]"></input>
    </p>
    <?php
}
// SAVING JAVNE NABAVKE CUSTOM POST VALUES ENTERED
function save_nabavke_meta( $post_id ) {
    // verify nonce
    if ( !wp_verify_nonce( $_POST['nabavke_meta_box_nonce'], basename(__FILE__) ) ) {
        return $post_id;
    }
    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return $post_id;
    }
    // check permissions
    if ( 'page' === $_POST['javne_nabavke'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        } elseif ( !current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
    }
    $old = get_post_meta( $post_id, 'nabavke_fields', true );
    $new = $_POST['nabavke_fields'];
    if ( $new && $new !== $old ) {
        update_post_meta( $post_id, 'nabavke_fields', $new );
    } elseif ( '' === $new && $old ) {
        delete_post_meta( $post_id, 'nabavke_fields', $old );
    }
}
add_action( 'save_post', 'save_nebavke_meta' );


function my_rewrite_flush() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush' );

endif; ?>
