<?php
/**
 * The template used for displaying secondary content
 *
 * @package WordPress
 * @subpackage zzjz
 * @since zzjz
 */
?>

	<section id="secondary">
		<div class="container">
			<div class="row">
				<!-- STATISTIKA -->
				<article class="col-xs-12 col-sm-6 col-md-3">
					<p><i class="fa fa-area-chart fa-2x" aria-hidden="true"></i></p>
					<h4>Statistika</h4>
					<p>Cras mattis consectetur purus sit amet fermentum. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Cras mattis consectetur purus sit amet fermentum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
					<a href="#" class="small btn btn-default">Informišite se &raquo;</a>
				</article><!-- /STAT -->

				<div class="clearfix visible-xs"></div><hr class="visible-xs" />

				<!-- REFERENTNA LABORATORIJA -->
				<article class="col-xs-12 col-sm-6 col-md-3">
					<p><i class="fa fa-medkit fa-2x"></i></p>
					<h4>Referentna laboratorija</h4>
					<p>Zavod ima jednu mikrobiološku laboratoriju u Zapadnobačkom okrugu, čije usluge koriste pacijenti sva četri doma zdravlja i Regionalna bolnica u Somboru.</p>
					<a href="<?php echo get_page_link(368); ?>" class="small btn btn-default">Pročitajte više &raquo;</a>
				</article><!-- /REF.LAB. -->

				<div class="clearfix visible-xs visible-sm"></div><hr class="visible-xs visible-sm" />

				<!-- ČLANOVIMA -->
        <article class="col-xs-12 col-sm-6 col-md-3">
					<p><i class="fa fa-user-md fa-2x"></i></p>
					<h4>Članovima srpskog lekarskog društva</h4>
					<p>Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					<a href="<?php echo get_page_link(4041); ?>" class="small btn btn-default">Pročitajte saopštenje &raquo;</a>
				</article><!-- /ČLANOVIMA -->

				<div class="clearfix visible-xs"></div><hr class="visible-xs" />

				<article class="col-xs-12 col-sm-6 col-md-3 uv-polen">
					<p><i class="fa fa-external-link fa-2x"></i></p>
					<h4>Merenje polena i prognoza UV indeksa</h4>
					<ul class="fa-ul">
						<li><i class="fa fa-li fa-sun-o fa-spin"></i><a href="http://www.hidmet.gov.rs/ciril/prognoza/uv1.php" target="_blank">Prognoza <abbr title="ultraljubičasto zračenje">UV</abbr> indeksa</a></li>
						<li><i class="fa fa-li fa-leaf"></i><a href="http://www.nspolen.com/sr/aeropalinoloski-izvestaji/sombor" target="_blank">Rezultati merenja koncentracije polena</a></li>
					</ul>
					<footer>
						<p class="small alert alert-warning alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="fa fa-exclamation-circle"></i> Linkovi vode na stranice van našeg domena i otvoriće se u novom prozoru</p>
					</footer>
				</article><!-- /POLEN I UV -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /#secondary -->
