<?php
/**
* The Header for our theme
*
* Displays all of the <head> section and everything up till <div id="main">
*
* @package WordPress
* @subpackage zzjz
* @since zavod
*/
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <!-- USING LATEST RENDERING MODE FOR IE -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
  <meta name="description" content="Zavod za javno zdravlje je regionalna ustanova koja se bavi preventivnom zdravstvenom delatno&scaron;&cacute;u. Zavod ima jednu mikrobiolo&scaron;ku laboratoriju u Zapadnoba&ccaron;kom okrugu, &ccaron;ije usluge koriste pacijenti sva &ccaron;etri doma zdravlja i Regionalna bolnica u Somboru.">

  <title>
    <?php
      if (is_category()) {
        echo 'Kategorija: '; wp_title(''); echo ' &laquo; ';
      } elseif (function_exists('is_tag') && is_tag()) {
        single_tag_title('Arhiva oznaka za &quot;'); echo '&quot; &laquo; ';
      } elseif (is_archive()) {
        wp_title(''); echo ' Arhiva &laquo; ';
      } elseif (is_page()) {
        echo wp_title(''); echo ' &laquo; ';
      } elseif (is_search()) {
        echo 'Pretraga za &quot;'.wp_specialchars($s).'&quot; &laquo; ';
      } elseif (!(is_404()) && (is_single()) || (is_page())) {
        wp_title(''); echo ' &laquo; ';
      } elseif (is_404()) {
        echo 'Došlo je do greške &laquo; ';
      } bloginfo('name');
    ?>
  </title>

  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

  <!-- CDNs -->
  <!-- Boostrap 3.3.7 -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" /> -->
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/css/bootstrap.min.zzjz.css" />
  <!-- Font Awesome -->
  <!-- <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet" /> -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" /> -->
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/css/font-awesome.min.css" />
  <!-- ZZJZ CSS (handcrafted) -->
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/css/search.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- GOOGLE MAPS SCRIPT; ONLY ON CONTACT PAGE -->
  <?php if(is_page('kontakt')) : ?>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script>
    function initialize() {
      var map_canvas = document.getElementById('map_canvas');
      var latlng = new google.maps.LatLng(45.779133, 19.110807);
      var map_options = {
        center: latlng,
        zoom: 16,
        zoomControl: 0,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDoubleClickZoom: 1,
        disableDefaultUI: 1,
        scrollwheel: 0,
        styles:  [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":60}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"lightness":30}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ef8c25"},{"lightness":40}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#b6c54c"},{"lightness":40},{"saturation":-40}]},{}]
      }
      var map = new google.maps.Map(map_canvas, map_options);
      google.maps.event.addDomListener(window, "resize", function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
      });
      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: "Zavod za javno zdravlje"
      });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  <?php endif; ?>

  <?php wp_head(); ?>
</head>

<body <?php body_class('nav-is-fixed'); ?>>

  <a href="#content" class="sr-only hidden-print">Idi na sadržaj</a>

  <header class="navbar navbar-default hidden-print navbar-fixed-top" role="banner" id="navigation">

    <div class="header-bar">
      <div class="container">
        <div class="row">
          <div class="col-md-9 col-sm-9 hidden-xs">
            <p class="small">Dobrodošli na internet prezentaciju Zavoda za javno zdravlje Sombor.<span class="hidden-sm"> Danas je <?php echo date('j.n.Y.'); ?></span></p>
          </div>
          <div class="col-md-3 col-sm-3 cd-main-header">
            <ul class="cd-header-buttons text-right">
              <li><a class="cd-search-trigger" href="#cd-search"><span class="text-hide">Pretraga</span><i class="fa fa-lg fa-search"></i></a></li>
            </ul><!-- cd-header-buttons -->
          </div>
        </div>
      </div>
    </div>
    <nav class="container" role="navigation">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainnav">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand text-hide" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>
      </div><!-- /NAV LOGO -->
      <?php
      wp_nav_menu( array(
        'menu'              => 'Glavna navigacija',
        'theme_location'    => 'primary',
        'depth'             => 2,
        'container'         => 'div',
        'container_class'   => 'navbar-collapse collapse hidden-print',
        'container_id'      => 'mainnav',
        'menu_class'        => 'nav navbar-nav navbar-right',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker())
      );
      ?>
    </nav>
  </header><!-- /NAVIGATION -->

  <div class="cd-overlay"></div>
  <div id="cd-search" class="cd-search">
    <?php get_search_form(); ?>
  </div>
