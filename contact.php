<?php
/*
 *
 * Template Name: Kontakt stranica
 *
 * @package WordPress
 * @subpackage zzjz
 * @since zavod
 *
 */

get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="main">
		<div id="single" class="contact">

			<?php the_title( '<h1 class="entry-title container">', '</h1>' ); ?>

			<section id="details">
				<div class="container">
					<address itemscope itemtype="http://schema.org/ContactPoint" class="vcard">
						<!-- POSTAL ADDRESS -->
						<section id="mailing-address">
							<div class="row">
								<div class="col-sm-2 col-sm-offset-1 heading">
									<h2>Adresa</h2>
								</div>
								<div itemscope itemtype="schema.org/PostalAddress" class="col-sm-9 data">
									<ul class="adr list-unstyled">
										<li><span itemprop="streetAddress" class="street-address">Vojvođanska 47</span></li>
										<li><span itemprop="postalCode" class="postal-code">25000</span> <span class="locality">Sombor</span></li>
										<li class="hidden"><span itmeprop="addressCountry" class="country-name">Srbija</span></li>
									</ul>
								</div><!-- /DATA COL -->
							</div><!-- /POSTAL ADDRESS -->
						</section>
						<!-- TELEPHONE -->
						<section id="telephone">
							<div class="row">
								<div class="col-sm-2 col-sm-offset-1 heading">
									<h2>Telefon</h2>
								</div>
								<div class="col-sm-9 data">
									<ul class="list-unstyled">
										<?php $phone = eae_encode_emails("025/412-888"); ?>
										<li class="tel"><span itemprop="faxNumber"><?php echo $phone; ?></span> (tel/fax)</li>
										<!-- <?php $phone = eae_encode_emails("025/412-225"); ?> -->
										<!-- <li class="tel"><span itemprop="telephone" class="fax"><?php echo $phone; ?></span> (tel)</li> -->
									</ul>
								</div><!-- /DATA COL -->
							</div><!-- /TELEPHONE -->
						</section>
						<!-- E-MAILS -->
						<section id="email">
							<div class="row vcard">
								<div class="col-sm-2 col-sm-offset-1 heading">
									<h2>e-mail</h2>
								</div>
								<div class="col-sm-9 data">
									<ul class="list-unstyled">
										<?php $email = eae_encode_emails("office@zzjzsombor.org"); ?>
										<li><a href="mailto:<?php echo $email; ?>?Subject=Poruka sa internet prezentacije"><?php echo $email; ?></a></li>
										<?php $email = eae_encode_emails("mikrobiologija@zzjzsombor.org"); ?>
										<li><a href="mailto:<?php echo $email; ?>?Subject=Poruka sa internet prezentacije"><?php echo $email; ?></a></li>
										<?php $email = eae_encode_emails("promocija@zzjzsombor.org"); ?>
										<li><a href="mailto:<?php echo $email; ?>?Subject=Poruka sa internet prezentacije"><?php echo $email; ?></a></li>
										<?php $email = eae_encode_emails("higijena@zzjzsombor.org"); ?>
										<li><a href="mailto:<?php echo $email; ?>?Subject=Poruka sa internet prezentacije"><?php echo $email; ?></a></li>
										<?php $email = eae_encode_emails("epidemiologija@zzjzsombor.org"); ?>
										<li><a href="mailto:<?php echo $email; ?>?Subject=Poruka sa internet prezentacije"><?php echo $email; ?></a></li>
										<?php $email = eae_encode_emails("informatika@zzjzsombor.org"); ?>
										<li><a href="mailto:<?php echo $email; ?>?Subject=Poruka sa internet prezentacije"><?php echo $email; ?></a></li>
									</ul>
								</div><!-- /DATA COL -->
							</div><!-- /E-MAILS -->
						</section>
					</address><!-- /MICRODATA -->
				</div>
			</section><!-- /CONTACT DETAILS -->

			<section id="map">
				<!-- <div id="map_canvas"></div> -->
			</section>

		</div><!-- /CONTAINER -->
	</article><!-- /PAGE -->

	<?php endwhile; endif; ?>
	<?php wp_reset_query(); ?>

<?php get_footer(); ?>
